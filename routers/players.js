const router = require('express').Router()
const emailValidator = require("email-validator");
const Player = require('../models/Player.js')
const enums = require('../utils/enums.js') 
const { ErrorFormat } = require('../errors/ErrorFormat.js')
const moment = require('moment');

router.get('/', (req, res, next) => { 
    let limitQuery = parseInt(req.query.limit)
    let pg = parseInt(req.query.page)
    
    let limit = (limitQuery > 0 && limitQuery < 21) ? limitQuery : (limitQuery >= 21) ? 20 : 10
    let pageQuery = (pg > 0) ? pg-1 : 0
    let page = limit * pageQuery

    let sortQuery = enums.sortPlayers(req.query.sort, req.query.reverse)
    let sort = (sortQuery !== null) ? sortQuery : {}

    let allPlayer = Player.find({}).limit(limit).skip(page).sort(sort)

    allPlayer.exec((err, players) => {
        if(!err){
            res.format({
                json: () => {
                    res.status(200).send(players)
                },
                html: () => {
                    if(players.length > 0){
                        res.render("players/players", {
                            title: "List of players",
                            allPlayers: players,
                            showAllPlayers: true,
                            moment: moment
                        })
                    }
                    else{
                        res.render("players/players", {
                            title: "List of players",
                            allPlayers: players,
                            showAllPlayers: false,
                            moment: moment
                        })
                    }
                }
            })
        }
        else{
            res.format({
                json: () => {
                    next(new ErrorFormat(500, "SERVEUR_ERROR", "Unknow error"))
                },
                html: () => {
                    res.render("error/error", {
                        type: "SERVEUR_ERROR",
                        status: 500,
                        message: "Unknow error",
                    })
                }
            })
        }
    })
})

router.post('/', (req, res, next) => {  
    const { name, email } = req.body 
    const emailValid = emailValidator.validate(email)

    // new player 
    let player = new Player({
        name: name,
        email: email,
        gameWin: 0,
        gameLost: 0,
        createdAt: new Date()
    })

    // check email formal
    if(emailValid){
        // check player already exist
        Player.findOne({email: email}, (err, result) => {
            if(result === null){
                // create player
                player.save((err, player) => {
                    if(!err){
                        res.format({
                            json: () => {
                                res.status(201).send(player)
                            },
                            html: () => {
                                res.redirect(301, `/players/${player._id}`)
                            }
                        })
                    }
                    else{
                        res.format({
                            json: () => {
                                next(new ErrorFormat(500, "SERVEUR_ERROR", "Unknow error"))
                            },
                            html: () => {
                                res.render("error/error", {
                                    type: "SERVEUR_ERROR",
                                    status: 500,
                                    message: "Unknow error"
                                })
                            }
                        })
                    }
                })
            }
            else{
                res.format({
                    json: () => {
                        next(new ErrorFormat(409, "CONFLICT", "Email already use"))
                    },
                    html: () => {
                        res.render("error/error", {
                            type: "CONFLICT",
                            status: 409,
                            message: "Email already use"
                        })
                    }
                })
            }
        })
    }
    else{
        res.format({
            json: () => {
                next(new ErrorFormat(400, "INVALID_FORMAT", "Email isn't valid"))
            },
            html: () => {
                res.render("error/error", {
                    type: "INVALID_FORMAT",
                    status: 400,
                    message: "Email isn't valid"
                })
            }
        })
    }
})

router.get('/new', (req, res, next) => {  
    res.format({
        json: () => {
            next(new ErrorFormat(409, "NOT_API_AVAILABLE", "Route doesn't exist"))
        },
        html: () => {
            res.render("players/new", {
                title: "Add a player",
                error: ""
            })
        }
    })
})

router.get('/:id', (req, res, next) => {  
    const id = req.params.id
    Player.findOne({_id: id}, (err, player) => {
        if(!err){
            if(player !== null){
                res.format({
                    json: () => {
                        res.status(200).send(player)
                    },
                    html: () => {
                        res.redirect(301, `/players/${player._id}/edit`)
                    }
                })
            }
            else{
                res.format({
                    json: () => {
                        next(new ErrorFormat(404, "NOT_FOUND", "Player not found"))
                    },
                    html: () => {
                        res.render("error/error", {
                            type: "NOT_FOUND",
                            status: 404,
                            message: "Player not found"
                        })
                    }
                })
            }
        }
        else{
            res.format({
                json: () => {
                    next(new ErrorFormat(404, "NOT_FOUND", "Player not found"))
                },
                html: () => {
                    res.render("error/error", {
                        type: "NOT_FOUND",
                        status: 404,
                        message: "Player not found"
                    })
                }
            })
        }
    })
})

router.get('/:id/edit', (req, res, next) => {  
    res.format({
        json: () => {
            next(new ErrorFormat(409, "NOT_API_AVAILABLE", "Route doesn't exist"))
        },
        html: () => {
            Player.findOne({_id: req.params.id}, (err, player) => {
                if(!err){
                    if(player !== null){
                        res.render("players/edit", {
                            title: "Editer un joueur",
                            id: player._id,
                            name: player.name,
                            email: player.email
                        })
                    }
                    else{
                        res.format({
                            html: () => {
                                res.render("error/error", {
                                    type: "NOT_FOUND",
                                    status: 404,
                                    message: "Player not found"
                                })
                            }
                        })
                    }
                }
                else{
                    res.format({
                        html: () => {
                            res.render("error/error", {
                                type: "SERVEUR_ERROR",
                                status: 500,
                                message: "Unknow error"
                            })
                        }
                    })
                }
            })
        }
    })
})

router.patch('/:id', (req, res, next) => {  
    const id = req.params.id
    const { name, email } = req.body
    
    let result = {}
    if(name != "" && name != null) result["name"] = name 
    if(email != ""  && email != null) {
        if(emailValidator.validate(email)){
            result["email"] = email 
        }
        else{
            res.format({
                json: () => {
                    next(new ErrorFormat(400, "INVALID_FORMAT", "Email isn't valid"))
                },
                html: () => {
                    res.render("error/error", {
                        title: "Edit a player",
                        error: "Invalid email"
                    })
                }
            })
        }
    }

    Player.findOneAndUpdate({_id: id}, result, (err, player) => {
        if(!err){
            res.format({
                json: () => {
                    res.status(200).send(player)
                },
                html: () => {
                    res.redirect(301, '/players')
                }
            })
        }
        else{
            res.format({
                json: () => {
                    next(new ErrorFormat(500, "SERVEUR_ERROR", "Unknow error"))
                },
                html: () => {
                    res.render("error/error", {
                        type: "SERVEUR_ERROR",
                        status: 500,
                        message: "Unknow error"
                    })
                }
            })
        }
    })
})

router.delete('/:id', (req, res, next) => {  
    const id = req.params.id
    Player.deleteOne({_id: id}, (err) =>{
        if(!err){
            res.format({
                json: () => {
                    res.status(204).end()
                },
                html: () => {
                    res.redirect(301, '/players')
                }
            })
        }
        else{
            res.format({
                json: () => {
                    next(new ErrorFormat(404, "NOT_FOUND", "Player not found"))
                },
                html: () => {
                    res.render("error/error", {
                        type: "NOT_FOUND",
                        status: 404,
                        message: "Player not found"
                    })
                }
            })
        }
    })
})

module.exports = router