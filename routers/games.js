const router = require('express').Router()
const Game = require('../models/Game.js')
const Player = require('../models/Player.js')
const GamePlayer = require('../models/GamePlayer.js')
const enums = require('../utils/enums.js')
const { ErrorFormat } = require('../errors/ErrorFormat.js')
const moment = require('moment');

router.get('/', (req, res, next) => {  
    let limitQuery = parseInt(req.query.limit)
    let pg = parseInt(req.query.page)
    
    let limit = (limitQuery > 0 && limitQuery < 21) ? limitQuery : (limitQuery >= 21) ? 20 : 10
    let pageQuery = (pg > 0) ? pg-1 : 0
    let page = limit * pageQuery

    let sortQuery = enums.sortGames(req.query.sort, req.query.reverse)
    let sort = (sortQuery !== null) ? sortQuery : {}

    let status = enums.status(req.query["f.status"])
    let find = (status !== null) ? {status: status} : {}

    let allGames = Game.find(find).limit(limit).skip(page).sort(sort)

    allGames.exec((err, games) => {
        if(!err){
            res.format({
                json: () => {
                    res.status(200).send(games)
                },
                html: () => {
                    if(games.length > 0){
                        res.render("games/games", {
                            title: "List of games",
                            allGames: games,
                            showAllGames: true,
                            moment: moment
                        })
                    }
                    else{
                        res.render("games/games", {
                            title: "List of games",
                            allGames: games,
                            showAllGames: false,
                            moment: moment
                        })
                    }
                }
            })
        }
        else{
            res.format({
                json: () => {
                    next(new ErrorFormat(500, "SERVEUR_ERROR", "Unknow error"))
                },
                html: () => {
                    res.render("error/error", {
                        type: "SERVEUR_ERROR",
                        status: 500,
                        message: "Unknow error"
                    })
                }
            })
        }
    })
})

router.post('/', (req, res, next) => {  
    const { name, mode } = req.body

    if(enums.mode(mode) !== null){
        let game = new Game({
            name: name,
            mode: mode,
            currentPlayerId: null,
            status: 'draft',
            createdAt: new Date()
        })
        // check game already exist
        Game.findOne({name: name}, (err, result) => {
            if(result === null){
                game.save((err, game) => {
                    if(!err){
                        res.format({
                            json: () => {
                                res.status(201).send(game)
                            },
                            html: () => {
                                res.redirect(301, `/games/${game._id}`)
                            }
                        })
                    }
                    else{
                        res.format({
                            json: () => {
                                next(new ErrorFormat(500, "SERVEUR_ERROR", "Unknow error"))
                            },
                            html: () => {
                                res.render("error/error", {
                                    type: "SERVEUR_ERROR",
                                    status: 500,
                                    message: "Unknow error"
                                })
                            }
                        })
                    }
                })
            }
            else{
                res.format({
                    json: () => {
                        next(new ErrorFormat(409, "CONFLICT", "Game name already use"))
                    },
                    html: () => {
                        res.render("error/error", {
                            type: "CONFLICT",
                            status: 409,
                            message: "Game name already use"
                        })
                    }
                })
            }
        })
    }
    else{
        res.format({
            json: () => {
                next(new ErrorFormat(400, "INVALID_FORMAT", "Mode isn't valid"))
            },
            html: () => {
                res.render("error/error", {
                    type: "INVALID_FORMAT",
                    status: 400,
                    message: "Mode isn't valid"
                })
            }
        })
    }
})

router.get('/new', (req, res, next) => {  
    res.format({
        json: () => {
            next(new ErrorFormat(409, "NOT_API_AVAILABLE", "Route doesn't exist"))
        },
        html: () => {
            res.render("games/new", {
                title: "Add new game",
                error: ""
            })
        }
    })
})

router.get('/:id', (req, res, next) => {  
    const id = req.params.id
    const include = req.query.include
    if(enums.include(include) !== null){
        console.log("include true")
    }
    else{
        Game.findOne({_id: id}, (err, game) => {
            if(!err){
                if(game !== null){
                    res.format({
                        json: () => {
                            res.status(200).send(game)
                        },
                        html: () => {
                            res.redirect(301, `/games/${game._id}/edit`)
                        }
                    })
                }
                else{
                    res.format({
                        json: () => {
                            next(new ErrorFormat(404, "NOT_FOUND", "Game not found"))
                        },
                        html: () => {
                            res.render("error/error", {
                                type: "NOT_FOUND",
                                status: 404,
                                message: "Game not found"
                            })
                        }
                    })
                }
            }
            else{
                res.format({
                    json: () => {
                        next(new ErrorFormat(500, "SERVEUR_ERROR", "Unknow error"))
                    },
                    html: () => {
                        res.render("error/error", {
                            type: "SERVEUR_ERROR",
                            status: 500,
                            message: "Unknow error"
                        })
                    }
                })
            }
        })
    }
})

router.get('/:id/edit', (req, res, next) => {  
    const id = req.params.id
    res.format({
        json: () => {
            next(new ErrorFormat(409, "NOT_API_AVAILABLE", "Route doesn't exist"))
        },
        html: () => {
            Game.findOne({_id: id}, (err, game) => {
                if(!err){
                    if(game !== null){
                        res.render("games/edit", {
                            title: "Edit a game",
                            id: game._id,
                            name: game.name,
                            mode: game.mode,
                            status: game.status
                        })
                    }
                    else{
                        res.render("error/error", {
                            type: "NOT_FOUND",
                            status: 404,
                            message: "Game not found"
                        })
                    }
                }
                else{
                    res.format({
                        html: () => {
                            res.render("error/error", {
                                type: "SERVEUR_ERROR",
                                status: 500,
                                message: "Unknow error"
                            })
                        }
                    })
                }
            })
        }
    })
})

router.patch('/:id', (req, res, next) => {  
    const id = req.params.id
    const { name, mode, status } = req.body

    let result = {}
    if(name != "" && name != null) result["name"] = name 
    if(mode != "" && mode != null) {
        if(enums.mode(mode) !== null){
            result["mode"] = mode 
        }
        else{
            next(new ErrorFormat(400, "INVALID_FORMAT", "Mode isn't valid"))
        }
    }
    if(status != "" && status != null) {
        if(enums.status(status) !== null){
            result["status"] = status 
        }
        else{
            next(new ErrorFormat(400, "INVALID_FORMAT", "Status isn't valid"))
        }
    }

    Game.findOneAndUpdate({_id: id}, result, (err, game) => {
        if(!err){
            res.format({
                json: () => {
                    res.status(200).send(game)
                },
                html: () => {
                    res.redirect(301, '/games/'+id)
                }
            })
        }
        else{
            res.format({
                json: () => {
                    next(new ErrorFormat(500, "SERVEUR_ERROR", "Unknow error"))
                },
                html: () => {
                    res.render("error/error", {
                        type: "SERVEUR_ERROR",
                        status: 500,
                        message: "Unknow error"
                    })
                }
            })
        }
    })
})

router.delete('/:id', (req, res, next) => {  
    const id = req.params.id
    Game.deleteOne({_id: id}, (err) => {
        if(!err){
            res.format({
                json: () => {
                    res.status(204).end()
                },
                html: () => {
                    res.redirect(301, '/games')
                }
            })
        }
        else{
            res.format({
                json: () => {
                    next(new ErrorFormat(404, "NOT_FOUND", "Game not found"))
                },
                html: () => {
                    res.render("error/error", {
                        type: "NOT_FOUND",
                        status: 404,
                        message: "Game not found"
                    })
                }
            })
        }
    })
})

router.get('/:id/players', (req, res, next) => {  
    const { id } = req.params

    GamePlayer.find({gameId: id}, async (err, listGamePlayer) => {
        if(!err){
            if(listGamePlayer !== null){
                let listPlayers = []

                for(let i= 0; i < listGamePlayer.length; i++){
                    let player = Player.find({_id: listGamePlayer[i].playerId})
                    await player.exec().then((result) => {
                        listPlayers.push(result[0])
                    })
                }
                res.format({
                    json: () => {
                        res.status(200).send(listPlayers)
                    },
                    html: () => {
                        res.render("gamesPlayers/players", {
                            title: "List of players in game",
                            allPlayers: listPlayers,
                            gameId: id
                        })
                    }
                })
            }
            else{
                res.format({
                    json: () => {
                        next(new ErrorFormat(404, "NOT_FOUND", "Game not found"))
                    },
                    html: () => {
                        res.render("error/error", {
                            type: "NOT_FOUND",
                            status: 404,
                            message: "Game not found"
                        })
                    }
                })
            }
        }
        else{
            res.format({
                json: () => {
                    next(new ErrorFormat(500, "SERVEUR_ERROR", "Unknow error"))
                },
                html: () => {
                    res.render("error/error", {
                        type: "SERVEUR_ERROR",
                        status: 500,
                        message: "Unknow error"
                    })
                }
            })
        }
    })
})

router.post('/:id/players', (req, res, next) => {  
    const playersForm = req.body.players
    const playersList = req.body
    let players = (playersForm != null) ? [playersForm] : playersList
    const { id } = req.params

    // Find game
    Game.findOne({_id: id}, (err, game) => {
        if(!err){
            if(game !== null){
                // Check if status is draft
                if(game.status === 'draft'){
                    // Find players
                    Player.find({ _id: {$in : players}}, (err, listPlayer) =>{
                        if(!err){
                            // Insert each player
                            listPlayer.forEach( player => {
                                let gamePlayer = new GamePlayer({
                                    playerId: player.id,
                                    gameId: id,
                                    remainingShots: null,
                                    score: 0,
                                    rank: null,
                                    order: null,
                                    inGame: true,
                                    createdAt: new Date()
                                })
                                gamePlayer.save((err) => {
                                    if(err){
                                        res.format({
                                            json: () => {
                                                next(new ErrorFormat(500, "SERVEUR_ERROR", "Unknow error"))
                                            },
                                            html: () => {
                                                res.render("error/error", {
                                                    type: "SERVEUR_ERROR",
                                                    status: 500,
                                                    message: "Unknow error"
                                                })
                                            }
                                        })
                                    }
                                })
                                
                            })
                            res.format({
                                json: () => {
                                    res.status(204).end()
                                },
                                html: () => {
                                    res.redirect(301, `/games/${id}/players`)
                                }
                            })
                        }
                        else{
                            res.format({
                                json: () => {
                                    next(new ErrorFormat(500, "SERVEUR_ERROR", "Unknow error"))
                                },
                                html: () => {
                                    res.render("error/error", {
                                        type: "SERVEUR_ERROR",
                                        status: 500,
                                        message: "Unknow error"
                                    })
                                }
                            })
                        }
                    })
                }
                else{
                    res.format({
                        json: () => {
                            next(new ErrorFormat(422, "PLAYERS_NOT_ADDABLE_GAME_STARTED", "This game has already started"))
                        },
                        html: () => {
                            res.render("error/error", {
                                type: "PLAYERS_NOT_ADDABLE_GAME_STARTED",
                                status: 422,
                                message: "This game has already started"
                            })
                        }
                    })
                }
            }
            else{
                res.format({
                    json: () => {
                        next(new ErrorFormat(404, "NOT_FOUND", "Game not found"))
                    },
                    html: () => {
                        res.render("error/error", {
                            type: "NOT_FOUND",
                            status: 404,
                            message: "Game not found"
                        })
                    }
                })
            }
        }
        else{
            res.format({
                json: () => {
                    next(new ErrorFormat(500, "SERVEUR_ERROR", "Unknow error"))
                },
                html: () => {
                    res.render("error/error", {
                        type: "SERVEUR_ERROR",
                        status: 500,
                        message: "Unknow error"
                    })
                }
            })
        }
    })
})

router.delete('/:id/players', (req, res, next) => {  
    const id = req.params.id
    let playerId = req.query.id
    if(!Array.isArray(playerId)){
        playerId = [playerId]
    }

    
    Game.findOne({_id: id}, (err, game) => {
        if(!err){
            if(game != null){
                if(game.status === 'draft'){
                    GamePlayer.deleteMany({gameId: id, playerId: {$in : playerId}}, (err) => {
                        if(!err){
                            res.format({
                                json: () => {
                                    res.status(204).end()
                                },
                                html: () => {
                                    res.redirect(301, '/games/'+id+'/players')
                                }
                            })
                        }
                        else{
                            res.format({
                                json: () => {
                                    next(new ErrorFormat(500, "SERVEUR_ERROR", "Unknow error"))
                                },
                                html: () => {
                                    res.render("error/error", {
                                        type: "SERVEUR_ERROR",
                                        status: 500,
                                        message: "Unknow error"
                                    })
                                }
                            })
                        }
                    })
                }
                else{
                    res.format({
                        json: () => {
                            next(new ErrorFormat(422, "PLAYERS_NOT_REMOVABLE_GAME_STARTED", "This game has already started"))
                        },
                        html: () => {
                            res.render("error/error", {
                                type: "PLAYERS_NOT_REMOVABLE_GAME_STARTED",
                                status: 422,
                                message: "This game has already started"
                            })
                        }
                    })
                }
            }
            else{
                res.format({
                    json: () => {
                        next(new ErrorFormat(404, "NOT_FOUND", "Game not found"))
                    },
                    html: () => {
                        res.render("error/error", {
                            type: "NOT_FOUND",
                            status: 404,
                            message: "Game not found"
                        })
                    }
                })
            }
        }
        else{
            res.format({
                json: () => {
                    next(new ErrorFormat(500, "SERVEUR_ERROR", "Unknow error"))
                },
                html: () => {
                    res.render("error/error", {
                        type: "SERVEUR_ERROR",
                        status: 500,
                        message: "Unknow error"
                    })
                }
            })
        }
    })
})

router.post('/:id/shots', (req, res, next) => {  
    const id = req.params.id
    res.end()
})

router.delete('/:id/shots/previous', (req, res, next) => {  
    const id = req.params.id
    res.end()
})



module.exports = router