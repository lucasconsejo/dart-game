const router = require('express').Router()
const gameRouter = require('./games.js')
const playerRouter = require('./players.js')
const { ErrorFormat } = require('../errors/ErrorFormat.js')

router.use('/games', gameRouter)
router.use('/players', playerRouter)

router.get('/', (req, res, next) => {  
    res.format({
        json: () => {
            throw new ErrorFormat(406, "NOT_API_AVAILABLE", "Route doesn't exist")
        },
        html: () => {
            res.redirect(301, "/games")
        }
    })
    res.end()
})

module.exports = router;