class ErrorFormat extends Error{
    constructor(status, type, message){
        super()
        this.status = status
        this.type = type
        this.message = message
    }
}

const handleError = (err, res) => {
    const { status, type, message } = err;
    res.status(status).json({
      error: {
        type: type,
        message: message
      }
    })
}

module.exports = {
    ErrorFormat,
    handleError
}