const express = require('express')
const app = express()
const router = require('./routers/routes.js')
const { handleError } = require('./errors/ErrorFormat.js')
const path = require('path')
const bodyParser = require('body-parser')
const methodOverride = require('method-override')
const port = process.env.APP_PORT || 3000

app.set('views', path.join(__dirname, 'views'));
app.engine('ejs', require('ejs-locals'));
app.set('view engine', 'ejs');

app.use(express.static(path.join(__dirname, 'assets')))

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

app.use(methodOverride('_method'))

app.use('/', router);

app.use((err, req, res, next) => {
  handleError(err, res)
})

app.listen(port, function () {
  console.log(`Server started on port ${port}`)
})