# Dart game - API

⚠️ **Warning** ⚠️  
Lier l'api au moteur ❌ 

## Installation

### Avec docker

Necessaire :
- Avoir installer `docker` et `docker-compose`

Démarrer les conteneurs `MongoDB` et `l'api` :

```sh
    docker-compose up -d
```

### Sans docker

Necessaire :
- Avoir un serveur `MongoDB` de démarrer sur votre machine
- Avoir installer `NodeJS`

Installer les dépendences `node_modules` :

```sh
    npm install
```

Démarrer l'api :

```sh
    npm start
```

## Routes invalides (non-fait)

- POST /games/{id}/shots
- DELETE /games/{id}/shots
 