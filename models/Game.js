const mongo = require('../db/Mongo.js')

const gameSchema = mongo.Schema({
  mode: String, 
  name: String,
  currentPlayerId: null | String | Number,
  status: String,
  createdAt: Date
}, {versionKey: false}); 

var Game = mongo.model('Game', gameSchema, 'games');

module.exports = Game