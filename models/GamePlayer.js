const mongo = require('../db/Mongo.js')

const gamePlayerSchema = mongo.Schema({
  playerId: Number | String,// Le player doit exister
  gameId: Number | String, // La game doit exister
  remainingShots: Number | null, // Nombre de coup restant sur le tour de jeu 
  score: Number,  
  rank: null | Number, // La position de l'utilisateur à la fin de la partie
  order: Number | null, // NULL par défaut, mais un ordre aléatoire estassigné au démarrage de partie, commence à 0  
  inGame: Boolean, // True au début, passe à False lorsque le joueur ne joueplus (ex: il a gagné, et les autres continuent)  
  createdAt: Date
}); 

var GamePlayer = mongo.model('GamePlayer', gamePlayerSchema, 'gamePlayer');

module.exports = GamePlayer
