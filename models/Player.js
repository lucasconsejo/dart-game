const mongo = require('../db/Mongo.js')

const playerSchema = mongo.Schema({
  name: String,  
  email: String,
  gameWin: Number,  
  gameLost: Number,  
  createdAt: Date 
}, {versionKey: false}); 

var Player = mongo.model('Player', playerSchema, 'players');

module.exports = Player
