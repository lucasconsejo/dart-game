const mongo = require('../db/Mongo.js')

const gameShotSchema = mongo.Schema({
  id: Number | String,
  gameId: Number | String,
  playerId: Number | String,
  multiplicator: Number, // 1, 2, 3  
  sector: Number, // BullEye = 25, en dehors = 0  
  createdAt: Date
}); 

module.exports = gameShotSchema
