const Game = require('../Game')

class Mode1 extends Game{
    
    constructor(players, nbSecteurs, endGame) { 
        super(players, nbSecteurs, endGame)
        this.secteurs = []
    }
    
    // Init la partie
    init(){
        for(let i = 0; i < this.players.length; i++){
            this.secteurs.push(1)
        }
        this.randomPlayer()
    }

    // La partie
    async play(){
        this.init()

        // Boucle tant que la partie n'est pas fini
        while(!this.endGame){

            // Boucle sur chaque joueur
            for(let i = 0; i < this.players.length; i++){
                let player = this.players[i]
    
                // Boucle pour les 3 lancés
                for(let j = 0; j < 3; j++){
                    let secteur = await player.tir()
                    if(this.secteurs[i] == secteur){
                        this.secteurs[i] = this.secteurs[i]+1
                        player.scores++
                    }
                    if(this.secteurs[i] == this.nbSecteurs+1){
                        break
                    }
                }
                console.log(`Score de ${player.name} est : ${player.scores}`)

                // Break si le secteur touché est égal à 20
                if(this.secteurs[i] == this.nbSecteurs+1){
                    this.endGame = true
                    break
                }
            }
        }
        console.log("Fin de la partie !!")
        this.finaleScore(true)
    }
    
}
module.exports = Mode1