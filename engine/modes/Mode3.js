const Game = require('../Game')

class Mode3 extends Game{

    constructor(players, nbSecteurs, endGame) { 
        super(players, nbSecteurs, endGame)
    }


    // Init la partie
    init(){
        for(let i = 0; i < this.players.length; i++){
            let player = this.players[i]
            player['B'] = 0
            player['20'] = 0
            player['19'] = 0
            player['18'] = 0
            player['17'] = 0
            player['16'] = 0
            player['15'] = 0
        }
        this.randomPlayer()
    }

    // La partie
    async play(){
        this.init()

        // Boucle tant que la partie n'est pas fini
        while(!this.endGame){

            // Boucle sur chaque joueur
            for(let i = 0; i < this.players.length; i++){
                let player = this.players[i]
    
                // Boucle pour les 3 lancés
                for(let j = 0; j < 3; j++){
                    console.table(this.players[i])
                    this.endGame = true
                    break
                }

            }
        }
        console.log("Fin de la partie !!")
        this.finaleScore(true)
    }
}
module.exports = Mode3