const Game = require('../Game')

class Mode2 extends Game{
    constructor(players, nbSecteurs, endGame) { 
        super(players, nbSecteurs, endGame)
    }

    // Init la partie
    init(){
        for(let i = 0; i < this.players.length; i++){
            this.players[i].scores = 301
            this.players[i]["canShoot"] = true
        }
        this.randomPlayer()
    }

    canShootAllPlayer(){
        let counter = 0
        for(let i = 0; i < this.players.length; i++){
            if(!this.players[i].canShoot){
                counter++
            }
        }
        if(counter === this.players.length){
           return true
        }
        else{
            return false
        }
    }

    // La partie
    async play(){
        this.init()

        // Boucle tant que la partie n'est pas fini
        while(!this.endGame){

             // Boucle sur chaque joueur
            for(let i = 0; i < this.players.length; i++){
                let player = this.players[i]

                if(player.canShoot){
                    let lastScore =  player.scores
                    let newScore = 0
                    let previousScore = 0
                    for(let j = 0; j < 3; j++){
                        console.log(`Lancé ${j+1}`)
                        let tir = await player.tirWithMulti()
                        newScore = player.scores - tir[0]
        
                        if(newScore > 0){
                            if(newScore != 1){
                                this.players[i].scores = newScore
                                previousScore = newScore
                                this.formatPlayerArray()
                            }
                            else{
                                console.log(`Le score ne peut pas être égal à 1 \n${player.name} ne peut plus jouer`)
                                this.players[i].canShoot = false
                                if(j==2){
                                    this.players[i].scores = -1
                                }
                                else{
                                    this.players[i].scores = previousScore
                                }
                            }
                        }
                        else if(newScore === 0){
                            if(tir[1]){
                                this.players[i].scores = newScore
                                this.endGame = true
                                break
                            }
                            else{
                                console.log('La partie doit se finir par un double')
                                if(j==2){
                                    this.players[i].scores = lastScore
                                    this.formatPlayerArray()
                                }
                            }
                        }
                        else{
                            console.log('Le score est négatif')
                            if(j==2){
                                this.players[i].scores = lastScore
                                this.formatPlayerArray()
                            }
                        }
                    }
                    if(this.players[i].score == -1){
                        continue
                    }
                }
                else{
                    if(this.canShootAllPlayer()){
                        this.endGame = true
                        break
                    }
                    else{
                        console.log(`${player.name} ne peut plus jouer`)
                        continue
                    }
                }
            }
        }
        this.finaleScore(false)
    }
}
module.exports = Mode2