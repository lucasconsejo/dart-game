const shuffle = require('shuffle-array')

class Game{
   
    constructor(players){
        this.players = players
        this.nbSecteurs = 20
        this.endGame = false
    }

    // Ordre des joueurs (random)
    randomPlayer(){
        shuffle(this.players)
        console.log("Voici l'ordre des lancés:")
        console.table(this.players)
        console.log(`${this.players[0].name} commence !`)
    }

    formatPlayerArray(){
        for(let i = 0; i < this.players.length; i++){
            if(this.players[i].scores === -1){
                this.players[i].scores = 1
            }
        }
        console.table(this.players)
    }

    // Score final
    finaleScore(sort){
        if(sort){
            console.log("Scores final")
            console.table(this.players.sort((a, b) => (a.scores > b.scores) ? -1 : 1))
        }
        else{
            console.log("Scores final")
            let players = this.players.sort((a, b) => (a.scores > b.scores) ? 1 : -1)
            this.formatPlayerArray()
        }
    }
        
}
module.exports = Game