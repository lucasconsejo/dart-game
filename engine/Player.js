const inquirer = require('inquirer')

class Player{
    constructor(name){
        this.name = name
        this.scores = 0
    }   

    // Simple tir
    async tir(){
        return await inquirer
            .prompt([
                {
                    type: 'number',
                    name: 'secteur',
                    message: `${this.name} a touché quel secteur ?`
                }
            ])
            .then(answers => {
                const { secteur } = answers
                return secteur
        });
    }

    // Tir avec un multiplicateur
    async tirWithMulti(){
        return await inquirer
            .prompt([
                {
                    type: 'number',
                    name: 'secteur',
                    message: `${this.name} a touché quel secteur ?`
                },
                {
                    type: 'number',
                    name: 'carreau',
                    message: `Le carreau a un multiplicateur de combien ?`
                }
            ])
            .then(answers => {
                const { secteur, carreau } = answers
                if(isNaN(secteur) || isNaN(carreau)){
                    console.log("Saisie incorrect")
                    return this.tirWithMulti()
                }
                let result = []
                if(carreau > 0 && carreau <= 3){
                    result.push(secteur * carreau)
                    result.push(true)
                    return result
                }
                else if(carreau > 3){
                    console.log("Le multiplicateur maximum est 3")
                    console.log("Le secteur est donc multiplié par 3")
                    result.push(secteur * 3)
                    result.push(true)
                    return result
                }
                else{
                    result.push(secteur)
                    result.push(false)
                    return result
                }
        });
    }
}
module.exports = Player