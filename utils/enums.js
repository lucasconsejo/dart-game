module.exports = {

    mode: (mode) => {
        switch(mode){
            case 'around-the-world':
                return mode
            case '301':
                return mode
            case 'cricket':
                return mode
            default:
                return null
        }
    },

    status: (status) => {
        switch(status){
            case 'draft':
                return status
            case 'started':
                return status
            case 'ended':
                return status
            default:
                return null;
        }
    },

    include: (include) => {
        switch(include){
            case 'gamePlayers':
                return include
            default:
                return null;
        }
    },

    sortPlayers: (sort, reverse) => {
        switch(sort){
            case 'name':
                return (reverse !== null && reverse !== undefined) ? { name: -1 } : { name: 1 }
            case 'email':
                return (reverse !== null && reverse !== undefined) ? { email: -1 } : { email: 1 }
            case 'gameWin':
                return (reverse !== null && reverse !== undefined) ? { gameWin: -1 } : { gameWin: 1 }
            case 'gameLost':
                return (reverse !== null && reverse !== undefined) ? { gameLost: -1 } : { gameLost: 1 }
            default:
                return null;
        }
    },

    sortGames: (sort, reverse) => {
        switch(sort){
            case 'name':
                return (reverse !== null && reverse !== undefined) ? { name: -1 } : { name: 1 }
            case 'status':
                return (reverse !== null && reverse !== undefined) ? { status: -1 } : { status: 1 }
            default:
                return null;
        }
    },
}