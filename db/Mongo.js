const mongoose = require('mongoose');
const url = `mongodb://${process.env.DB_HOST || 'localhost'}:${process.env.DB_PORT || '27017'}/dart_game`

mongoose.connect(url, { 
  useNewUrlParser: true,
  useUnifiedTopology: true 
})
  
const conn = mongoose.connection

conn.on("open", () => { 
  console.log('Database connected !') 
})

conn.on('error', (err) => { 
  console.log('Connection database error: ', err)
})

module.exports = mongoose